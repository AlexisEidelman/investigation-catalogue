---
title: Catalogage de données - Investigation
separator: <!--s-->
verticalSeparator: <!--v-->
revealOptions:
    transition: 'fade'
---
# Catalogage de données

## Investigation 🔍

<img src="dinum-logo.png" width="30%">

<img src="jailbreak-logo.png" width="30%">

#### _Synthèse et feuille de route_

<!--v-->

###### [Présentation accessible en ligne](https://jailbreak.gitlab.io/investigation-catalogue/synthese.html) ([code source](https://gitlab.com/jailbreak/investigation-catalogue))

**Publication**  
9 septembre 2021  
**Mise à jour**  
18 octobre 2021

<!--s-->

## Contexte

* Chaque ministère a élaboré en 2021 sa feuille de route de la politique de la donnée.
* Le catalogage des données est identifié comme un pré-requis pour mener cette politique.
* Plusieurs administrations se sont déjà engagées dans cette démarche.
* La DINUM souhaite faciliter la démarche en outillant les administrations.

<!--s-->

## Avant de commencer...

### Quelques définitions

* **Jeu de données** : paquet contenant un ou plusieurs fichiers (également appelés "ressources")
* **Métadonnées** : informations sur un fichier (nom, description, date de publication...)
* **Schéma de données** : méthode pour spécifier et contrôler la structure et le contenu d'un fichier
* **Catalogue de données** : liste de jeux de données et leurs métadonnées (donc pas le contenu des données elles-mêmes ⚠️)

<!--v-->

## Et en pratique ?

Le schéma du catalogue de données ? ([en cours d'élaboration](https://github.com/etalab/schema-catalogue-donnees))  
Il pourrait ressembler à ça :

![](schema_catalogue_header.png)

Et le contenu du catalogue lui-même ?  
Exemple d'un jeu de données décrit suivant ce schéma :

![](schema_catalogue_content.png)

<!--s-->

## Catalogue *vs* Portail

10 ans d'*open data*,  
3 paradigmes qui se succèdent

<!--v-->

1️⃣  
#### *«&nbsp;Le catalogue **est** le portail&nbsp;»*

Aux origines de l'*open data*,  
toutes les données cataloguées sont publiées,  
dans un seul et même processus.

<!--v-->

2️⃣  
#### *«&nbsp;Il faut un catalogue<br>**avant** un portail&nbsp;»*

Plus tard, le processus de catalogage des données  
est considéré comme préalable à leur publication.

Les portails ne sont pas conçus pour ce nouveau paradigme   
de *data management* interne.  

Des outils dédiés apparaissent, les processus se scindent,  
sans véritable interaction entre les deux.

<!--v-->

3️⃣  
#### *«&nbsp;Il faut un catalogue<br>**avec** et **pour** le portail&nbsp;»*

Aujourd'hui, il est temps de renouer ce lien cassé entre  
catalogue et portail, les outils comme les processus.

En définitive, il s'agit de mettre le catalogage au service  
de la politique de la donnée, de son ouverture  
et de son partage intra- et inter-ministériel.

<!--s-->

## Catalogue 🤝 Portail

#### *Data management* interne
## ♻️
#### Publication et valorisation des données

<!--s-->

## Objectif de l'investigation

#### Comprendre les besoins fonctionnels à adresser pour cataloguer les données d'une administration.

<!--s-->

## Phasage

1. **Investigation**  
> 📍 **Vous êtes ici.**
2. Développement

<!--v-->

## 1. Investigation

* **Calendrier** : de juillet à octobre.
* **Objectif** : comprendre les besoins fonctionnels pour cataloguer les données d'une administration.

<!--v-->

## 2. Développement

* **Calendrier** : fin 2021.
* **Objectif** : outiller les administrations dans leur démarche de catalogage de données.

<!--s-->

## Méthodologie 

* Analyse de l’existant
  * Etat des lieux
  * Benchmark
  * Tests outils
* Etude du besoin
  * Entretiens
  * Ateliers

<!--s-->

## Chiffres-clés

### **9** entretiens

### **1** atelier schéma

### **1** atelier outil

### **15+** outils

<!--s-->

## Parties prenantes

* Agence de la transition écologique (ADEME)
* Direction générale des Finances publiques
* Direction interministérielle du numérique
* Ministère de la Culture
* Ministère de l'Europe et des Affaires étrangères
* Ministère de l'Intérieur
* Ministère des Solidarités et de la Santé
* Ministère de la Transition écologique
* Nantes Métropole

<!--s-->

## Entretiens

La synthèse se décline en trois dimensions :

* **Domaines** : les catégories qui forment le périmètre du catalogage ;
* **Défis** : les problèmes rencontrés par les utilisateurs ;
* **Besoins** : les fonctionnalités nécessaires pour les adresser.

<!--s-->

| Domaines               | Défis                                                                                                                   |
| ---------------------- | ----------------------------------------------------------------------------------------------------------------------- |
| Découvrabilité         | Les membres de l'organisation ne connaissent pas les données existantes                                                 |
| Traçabilité            | Les membres de l'organisation ne savent pas<br>1) qui est responsable de la donnée<br>2) de quel SI provient la donnée. |
| Maintenabilité         | Les organisations ont des difficultés à maintenir le catalogue à jour.                                                  |
| Modèle de données      | Chaque organisation utilise un outil / schéma différent.                                                                |
| Gouvernance            | Chaque organisation a des processus spécifiques.                                                                        |
| Ouverture              | Certaines données / métadonnées peuvent ou doivent être ouvertes.                                                       |
| Intégration SI         | Les membres de l'organisation n'ont pas un accès direct aux données dont ils ont besoin                                 |
| Expérience utilisateur | Les membres de l'organisation n'ont pas de compétences sur les données.                                                 |

<!--s-->

## Défis

👇 Les problèmes rencontrés par les parties prenantes 👇

<!--v-->

## Découvrabilité

#### Les membres de l'organisation<br>ne connaissent pas les données<br>existant en leur sein.

💬 *"la connaissance est dans ma tête"*

💬 *"des directions ont été étonnées de découvrir<br>en travaillant ensemble l'existence de leurs données"*

💬 *"un agent passe 40 mns par jour<br>à trouver une information"*

<!--v-->

## Traçabilité

#### Les membres de l'organisation ne savent pas<br>1) qui est responsable de la donnée<br>2) de quel SI provient la donnée.

💬 *"comme dans l'agriculture bio,<br>il faut pouvoir remonter la chaîne<br>du consommateur jusqu'au producteur"*

💬 *"à qui s'adresser<br>si je veux modifier la source ?"*

💬 *"il faut rendre clair de quelle application<br>provient telle donnée"*

<!--v-->

## Maintenabilité

#### Les organisations ont des difficultés à maintenir le catalogue à jour.

💬 *"les détenteurs des informations<br>ne peuvent pas les faire remonter"*

💬 *"il faut instrumenter<br>la gestion du catalogue et sa mise à jour"*

<!--v-->

## Modèle de données

#### Chaque organisation utilise un outil / schéma différent.

💬 *"les métadonnées collectées<br>par les organisations sont différentes"*

💬 *"il faut homogénéiser l'outillage"*

<!--v-->

## Gouvernance

#### Chaque organisation a des processus spécifiques.

💬 *"nos utilisateurs sont dans plusieurs organisations<br>(ministère, opérateurs, services déconcentrés...)"*

💬 *"chez nous, un responsable des données<br>doit obtenir une permission de son supérieur,<br>par exemple pour ouvrir les données "*

<!--v-->

## Ouverture

#### Certaines données / métadonnées peuvent ou doivent être ouvertes.

💬 *"le catalogue doit indiquer si<br>une donnée est sensible / non-exposable"*

💬 *"l"outil peut avoir une vue publique,<br>rien n'est sensible"*

<!--v-->

## Intégration SI

#### Les membres de l'organisation n'ont pas accès aux données dont ils ont besoin.

💬 *"les sources des données sont<br>des grosses applications et bases de données"*

💬 *"il est possible de récupérer<br>les flux de métadonnées<br>depuis nos applications métiers"*


<!--v-->

## Expérience utilisateur (UX)

#### Les membres de l'organisation n'ont pas de compétences sur les données.

💬 *"il faut que l'interface soit<br>plus intuitive qu'un tableur Excel"*

💬 *"on a besoin d'une méthodologie<br>et de bonnes pratiques à suivre"*

<!--s-->

## Besoins

👇 Les principales fonctionnalités requises 👇 

<!--v-->

## Axe 1 - Déploiement

#### Défis : Gouvernance, ouverture, intégration

* Permettre aux administrations de **créer leur catalogue** simplement.
* Gérer différents **niveaux d'accès** en lecture et en écriture.
* Permettre un accès **multi-catalogues** au sein du même outil.
* Connecter au portail *open data* pour faciliter la **gestion** du catalogue public.
* Connecter au système d'information interne pour **automatiser la récupération** des métadonnées.

<!--v-->

## Axe 2 - Alimentation

#### Défis : Maintenabilité, traçabilité,<br>modèle de données

* Donner un **accès utilisateur** à tous les membres au sein de l'organisation.
* Permettre de **déclarer** des jeux de données et d'en **décrire** les métadonnées (crowdsourcing interne).
* Définir pour chaque jeu de données les **contacts** et les **sources** (application, base de données...).
* Définir un **schéma de données** commun pour les catalogues.
* Permettre d'ajouter des **champs internes** à une organisation pour compléter le schéma de données commun.
* Permettre d'**importer** ou d'**exporter** tout ou partie du catalogue.

<!--v-->

## Axe 3 - Usages

#### Défis : Découvrabilité, expérience utilisateur

* Concevoir une interface pour des utilisateurs **sans compétence sur les données**.
* Permettre aux utilisateurs une **recherche simple** (plein texte ou structurée, par personne, service, mot-clé, description, couverture temporelle ou géographique...)
* Permettre aux utilisateurs de **s'abonner** à un jeu de données pour les avertir des changements.
* Publier des **bonnes pratiques** en matière de catalogage.

<!--s-->

## En résumé...

Les parties prenantes ont besoin d'un outil leur permettant de :
1. **Gérer** et **maintenir à jour** un catalogue de données ***interne***,
2. **Publier** un catalogue ***public*** de leurs données, 
3. **Consulter** les catalogues publics des **autres organisations**.

<!--s-->

## Benchmark

Les outils explorés et testés se divisent en trois catégories :

* Outil ***générique*** de gestion de données (à la Excel)
* Outil de gestion de catalogue de données ***interne*** (à la Apache Atlas)
* Outil de gestion de catalogue de données ***public*** (à la Datagouv)

👇

<!--v-->

## `DATA TABLES`

###### Outil ***générique*** de gestion de données

* [Airtable](https://airtable.com/)
* [Baserow](https://baserow.io)
* [Excel](https://microsoft.com/microsoft-365/excel)
* [Google Sheet](https://spreadsheets.google.com/)
* [Libre Office Calc](https://libreoffice.org/discover/calc/)
* [NocoDB](https://nocodb.com)

Permet de gérer manuellement  
un catalogue de données (public et/ou interne)  
de façon simple mais peu pérenne.

<!--v-->

## `DATA CATALOGS`

###### Outil de gestion de catalogue de données ***interne***<br>(également appelés outils de gestion de métadonnées ou de "master data")

* [Amundsen](https://amundsen.io/)
* [Apache Atlas](https://atlas.apache.org/)
* [Data Galaxy](https://datagalaxy.com/)
* [Informatica Data Catalog](https://informatica.com/products/data-catalog.html)
* [LinkedIn Datahub](https://datahubproject.io/)
* [Marquez](https://marquezproject.github.io/marquez/)
* [Netflix Metacat](https://github.com/Netflix/metacat)
* [Talend Data Catalog](https://www.talend.com/products/data-catalog/)

Permet d'automatiser la gestion d'un  
catalogue de données mais demande  
un fort investissement technique et d'intégration.

<!--v-->

## `DATA PORTALS`

###### Outil de gestion de catalogue de données ***public***

* [CKAN](https://ckan.org/)
* [OpenDataSoft](https://opendatasoft.com/)
* [uData](https://github.com/opendatateam/udata) ([Data.gouv.fr](https://data.gouv.fr/))

Permet de publier ou partager des données.

<!--v-->

## Comparaison

Facteurs de différenciation sur les besoins des parties prenantes

|                        | DATA TABLES | DATA CATALOGS | DATA PORTALS |
| ---------------------- | :---------: | :-----------: | :----------: |
| Découvrabilité         |     ❌      |      ✅       |      ✅      |
| Traçabilité            |     ❌      |      ✅       |      ❌      |
| Maintenabilité         |     ❌      |      ✅       |      ❌      |
| Modèle de données      |     ❌      |      ✅       |      ❌      |
| Gouvernance            |     ❌      |      ✅       |      ❌      |
| Ouverture              |     ❌      |      ❌       |      ✅      |
| Intégration SI         |     ❌      |      ✅       |      ❌      |
| Expérience utilisateur |     ✅      |      ❌       |      ✅      |

<!--s-->

## Conclusion

L'outil pressenti lors de la phase d'investigation  
présente les caractéristiques suivantes&nbsp;:

* Logiciel libre mis à disposition des administrations en mode SaaS par la DINUM&nbsp;;
* La possibilité de contribuer directement au catalogue à travers des fonctionnalités d'édition&nbsp;;
* L'intégration technique avec la plateforme [data.gouv.fr](https://data.gouv.fr) pour faciliter l'alimentation des catalogues&nbsp;;
* L'intégration technique rendue possible aux SI des organisations à travers des connecteurs et une API&nbsp;;
* La possibilité d'exposer une partie d'un catalogue à l'ensemble des acteurs interministériels&nbsp;;
* La possibilité de publier un catalogue de données en *open data* sur les portails dédiés.

<!--s-->

## Feuille de route

Traduire les conclusions de l'investigation en **cas d'usages** et **parcours**<br>afin de développer les fonctionnalités de l'outil.

<!--s-->

## Axe 1 - Déploiement

👇

<!--v-->

## Cas d'usage 1A

💬 En tant que responsable dans mon organisation,  
je veux créer un catalogue,  
afin de cataloguer les données de mon organisation.

#### Parcours 👇

Je demande la création d'un catalogue sur catalogue.data.gouv.fr,  
il est créé après vérification par la DINUM,  
et j'obtiens un accès **administrateur** au catalogue de mon organisation.

<!--v-->

## Cas d'usage 1B

💬 En tant qu'**administrateur**,  
je souhaite connecter le catalogue de mon organisation à data.gouv.fr,  
afin de le peupler avec les données déjà publiées en *open data*.

#### Parcours 👇

Je vais dans le back-office "Administration" de l'outil,  
où je peux connecter au compte "organisation" sur data.gouv.fr,  
et ajouter son contenu dans le catalogue.

<!--v-->

## Cas d'usage 1C

💬 En tant qu'**administrateur**,  
je veux que des membres de mon organisations qui sont dans des services extérieurs (e.g. services déconcentrés) puissent créer un compte utilisateur,    
afin qu'ils aient également accès au catalogue de l'organisation.

#### Parcours 👇

Je vais dans le back-office "Administration" de l'outil,  
où je peux gérer la liste des domaines (`*.gouv.fr`, `*.fr`, etc.) liés à mon organisation et permettant de créer un compte **utilisateur** donnant accès au catalogue.

<!--v-->

## Cas d'usage 1D

💬 En tant qu'**administrateur**,  
je souhaite gérer différents niveaux d'accès en lecture,  
afin de rendre certaines parties du catalogue de mon organisation accessibles au public et à d'autres organisations.

#### Parcours 👇

Je vais sur la page d'un jeu de données,  
où une option me permet de définir son niveau d'ouverture (Intra-organisation, Inter-organisation ou Public).

<!--s-->

## Axe 2 - Alimentation

👇

<!--v-->

## Cas d'usage 2A

💬 En tant que membre de mon organisation,  
je veux utiliser le catalogue de mon organisation,  
afin de connaître les données de mon organisation.

#### Parcours 👇

Je vais sur catalogue.data.gouv.fr, un compte est requis,  
je crée un compte **utilisateur** grâce à mon adresse email liée à l'organisation,  
qui me donne un accès au catalogue de mon organisation.

<!--v-->

## Cas d'usage 2B

💬 En tant qu'**utilisateur**,  
je souhaite ajouter un jeu de données au catalogue,  
afin d'en partager la connaissance dans mon organisation.

#### Parcours 👇

Je me connecte à l'outil avec mon compte **utilisateur**, je clique sur un bouton "Ajouter un jeu de données" qui me permet de décrire le jeu de données dans un formulaire.

<!--v-->

## Cas d'usage 2C

💬 En tant qu'**utilisateur**,  
je souhaite corriger ou compléter la page d'un jeu de données,  
afin d'en partager la connaissance dans mon organisation.

#### Parcours 👇

Je me connecte à l'outil avec mon compte **utilisateur**, je vais la page du jeu de données et je clique sur un bouton "Editer", ce qui ouvre un formulaire qui me permet de décrire le jeu de données.

<!--v-->

## Cas d'usage 2D

💬 En tant qu'**administrateur**,  
je souhaite supprimer la page d'un jeu de données,  
afin de nettoyer le contenu du catalogue (doublons, erreurs...).

#### Parcours 👇

Je me connecte à l'outil avec mon compte **administrateur**, je vais sur la page du jeu de données et clique sur le bouton "Supprimer" puis "Confirmer".

<!--v-->

## Cas d'usage 2E

💬 En tant qu'**administrateur**,  
je veux ajouter des champs métadonnées qui ne sont pas dans le schéma commun,  
fin de pouvoir décrire de façon plus spécifique les jeux de données.

#### Parcours 👇

Je me connecte à l'outil avec mon compte **administrateur**  
et je vais dans le back-office "Administration" de l'outil,  
où je peux gérer le schéma du catalogue interne à mon organisation, avec des champ personnalisé au catalogue de mon organisation.

<!--v-->

## Cas d'usage 2F

💬 En tant qu'**utilisateur**,  
je veux voir l'historique de la fiche d'un jeu de données,  
afin de savoir qui l'a modifiée et éventuellement revenir en arrière.

#### Parcours 👇
 
Je vais sur la page du jeu de données et clique sur le bouton "Historique".

<!--s-->

## Axe 3 - Usages

👇

<!--v-->

## Cas d'usage 3A

💬 En tant qu'**administrateur**,  
je souhaite créer une collection de jeux de données,  
afin de permettre aux utilisateurs de trouver plus facilement des données sur un thème particulier.

#### Parcours 👇

Je clique sur "Créer collection", ce qui me permet de lui donner un nom et une description, puis d'ajouter des fiches de jeux de données dans la collection. Les collections et leur contenu sont ensuite accessibles à l'ensemble des utilisateurs.

<!--v-->

## Cas d'usage 3B

💬 En tant qu'**utilisateur**,  
je souhaite trouver un jeu de données précis.

#### Parcours 👇

Je clique dans le champ de recherche et tape un mot ou une phrase, ce qui parcourt toutes les métadonnées (nom, description, mot-clés, service, contacts...).

<!--v-->

## Cas d'usage 3C

💬 En tant qu'**utilisateur**,  
je souhaite trouver un jeu de données sans critère précis.

#### Parcours 👇

Je clique sur "Recherche avancée", ce qui permet de filtrer par service ou par personne.

<!--v-->

## Cas d'usage 3D

💬 En tant qu'**utilisateur**,  
je souhaite exporter un sous-ensemble du catalogue,  
afin de pouvoir l'utiliser dans un autre outil comme Excel.

#### Parcours 👇

J'effectue une recherche selon mes propres critères, puis je clique sur "Exporter les résultats" ce qui me permet de télécharger au format CSV la liste des jeux de données filtrés par ma recherche.

<!--v-->

## Cas d'usage 3E

💬 En tant qu'**utilisateur**,  
je souhaite m'abonner à la fiche d'un jeu de données,  
afin d'être averti des modifications.

#### Parcours 👇

Je vais sur la page du jeu de données et clique sur "Suivre".
